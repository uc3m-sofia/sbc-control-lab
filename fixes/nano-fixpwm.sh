#!/bin/bash

#based on : https://forums.developer.nvidia.com/t/jetson-nano-pwm-not-working/154939/19

#PWM 2
sudo busybox devmem 0x6000d100 32 0x00
sudo busybox devmem 0x70003248 32 0x46

#PWM 0
sudo busybox devmem 0x6000d504 32 0x2
sudo busybox devmem 0x700031fc 32 0x45
