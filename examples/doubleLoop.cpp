
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 74;  // BOARD pin 5
const int pin_B = 216; // BOARD pin 7

const int pin_ain1 = 149;  // BOARD pin 29
const int pin_ain2 = 200; // BOARD pin 31
const int pin_stby = 13;   // BOARD pin 27
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
#include "fcontrol.h"
#include "IPlot.h"



//void (*const blinkPtr)(int);

using namespace std;

int main()
{

    double dts=0.01;

    GPIOJetson sbc;
    QuadEncoder enc(pin_A, pin_B, sbc, 28, dts);

    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);

    vector<vector<double>> G={ {0.8, 0} , {0.01, 1} };
    vector<vector<double>> H={ {2.4} , {0}};
    vector<vector<double>> C={ {0,1} };
    vector<vector<double>> D={ {0} };


    StateObserver obs(G,H,C,D,dts);
    obs.SetGainsK(vector<vector<double>> { {0.5} , {0.1} });

    vector<vector<double>> x={ {0} , {0} };


    drv.enable();


    SamplingTime Ts(dts); // en segundos

//    vector<double> num = {0,1};
//    vector<double> den = {-1,1};
//    SystemBlock P1(num,den);

    IPlot plot1(dts,"real"," x axis","y axis");
    IPlot plot2(dts,"obs"," x axis","y axis");

    PID C1(0.119,4.83,0,dts);
//    C1.AntiWindup(-100,100);
    PID C2(3.98,3.27,0.165,dts);

    double out=0,refv=0,act=0;
    double pos=0,vel=0;
    double errv=0, errp=0;
    double ref=200;

    for (int i=0;i<500;i++)
    {


        pos=enc.get_pos();
        vel=enc.get_vel();
        out=pos;

        errp = ref-pos;
        refv = errp > C2;

        //close the high level loop by commenting this line
        refv=400;

        errv = refv-vel;
        act = errv > C1;

        drv.SetThrottle(act);

        plot1.pushBack(vel);
        plot2.pushBack(pos);


        Ts.WaitSamplingTime();
    }
    drv.SetThrottle(0);

    plot1.Plot();
    plot2.Plot();


    cout << "enc stopped at pulse: " << enc.pulses << endl;






    return 0;
}
