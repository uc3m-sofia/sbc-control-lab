
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 74;  // BOARD pin 5
const int pin_B = 216; // BOARD pin 7

const int pin_ain1 = 149;  // BOARD pin 29
const int pin_ain2 = 200; // BOARD pin 31
const int pin_stby = 13;   // BOARD pin 27
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
#include "fcontrol.h"
#include "IPlot.h"



//void (*const blinkPtr)(int);

using namespace std;

int main()
{

    double dts=0.01;

    GPIOJetson sbc;
    QuadEncoder enc(pin_A, pin_B, sbc, 28, dts);

    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);

    vector<vector<double>> G={ {0.8, 0} , {0.01, 1} };
    vector<vector<double>> H={ {2.4} , {0}};
    vector<vector<double>> C={ {0,1} };
    vector<vector<double>> D={ {0} };


    StateObserver obs(G,H,C,D,dts);
    obs.SetGainsK(vector<vector<double>> { {0.5} , {0.1} });

    vector<vector<double>> x={ {0} , {0} };


    drv.enable();


    SamplingTime Ts(dts); // en segundos

//    vector<double> num = {0,1};
//    vector<double> den = {-1,1};
//    SystemBlock P1(num,den);

    IPlot plot1(dts,"real"," x axis","y axis");
    IPlot plot2(dts,"obs"," x axis","y axis");

    OnlineSystemIdentification model(0,1);


    double vel=0,pos=0;
    double in=60;

    drv.SetThrottle(in);

    for (int i=0;i<400;i++)
    {

        vel = enc.get_vel();
        pos = enc.get_pos();

        //in=50+(rand()% 10);
        model.UpdateSystem(in,vel);
        obs.Update(in,pos);

        x=obs.GetState();

        plot1.pushBack(pos);
        plot2.pushBack(x[1][0]);
        Ts.WaitSamplingTime();
    }

    model.PrintZTransferFunction(dts);
    drv.SetThrottle(0);

    plot1.Plot();
    plot2.Plot();

    SS model2(model.GetSystemBlock());

    model2.printSystem();


    cout << "enc stopped at pulses: " << enc.pulses << endl;
    cout << "enc stopped at pos: " << enc.get_pos() << " rad." << endl;






    return 0;
}
