
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 216; // BOARD pin 7
const int pin_B = 74;  // BOARD pin 5

//const int pin_ain1 = 149;  // BOARD pin 29
//const int pin_ain2 = 200; // BOARD pin 31
//const int pin_stby = 13;   // BOARD pin 27
//const int num_pwm = 2;   // BOARD pin 33

const int pin_ain1 = 200;   // BOARD pin 31
const int pin_ain2 = 149; // BOARD pin 29
const int pin_stby = 13;  // BOARD pin 27
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
#include "fcontrol.h"
#include "IPlot.h"



//void (*const blinkPtr)(int);

using namespace std;

int main()
{

    double dts=0.01;

    GPIOJetson sbc;

    QuadEncoder enc(pin_A, pin_B, sbc, 28, dts);

    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);



    drv.enable();
//    drv.SetThrottle(100);


    SamplingTime Ts(dts); // en segundos

    IPlot plot1(dts,"plot1"," x axis","y axis");
    IPlot plot2(dts,"plot2"," x axis","y axis");

    double out=0,in=0,pos=0;

    vector<vector<double>> x={{0},{0}};


    for (int i=0;i<400;i++)
    {

        in= 100*i*dts;
        drv.SetThrottle(in);


        out=enc.get_vel();


        plot1.pushBack(in);
        plot2.pushBack(out);

        Ts.WaitSamplingTime();
    }
    drv.SetThrottle(0);

    plot1.Plot();
    plot2.Plot();





    return 0;
}
