
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 74;  // BOARD pin 5
const int pin_B = 216; // BOARD pin 7

const int pin_ain1 = 149;  // BOARD pin 29
const int pin_ain2 = 200; // BOARD pin 31
const int pin_stby = 13;   // BOARD pin 27
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
#include "fcontrol.h"
#include "IPlot.h"



//void (*const blinkPtr)(int);

using namespace std;

int main()
{

    double dts=0.01;

    GPIOJetson sbc;
    QuadEncoder enc(pin_A, pin_B, sbc, 28, dts);

    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);

    vector<vector<double>> G={ {0.8, 0} , {0.01, 1} };
    vector<vector<double>> H={ {2.4} , {0}};
    vector<vector<double>> C={ {0,1} };
    vector<vector<double>> D={ {0} };


    StateObserver obs(G,H,C,D,dts);
    obs.SetGainsK(vector<vector<double>> { {0.5} , {0.1} });

    vector<vector<double>> x={ {0} , {0} };


    drv.enable();


    SamplingTime Ts(dts); // en segundos

//    vector<double> num = {0,1};
//    vector<double> den = {-1,1};
//    SystemBlock P1(num,den);

    IPlot plot1(dts,"real"," x axis","y axis");
    IPlot plot2(dts,"obs"," x axis","y axis");

//    PID C1(0.12,4.87,0,dts);
    PID C1(0.619,0.955,0,dts);
//    PID C1(0.87,1.54,0.043,dts);
    C1.AntiWindup(-100,100);
    PID C2(2,0.1,0,dts);

    double out=0,uv=0,up=0;
    double errv=0, errp=0;
    double ref=100;

    for (int i=0;i<500;i++)
    {

        obs.Update(up, enc.get_pos());
        x=obs.GetState();
        out=x[1][0]; //x[1][0] pos, x[0][0] vel
        out=enc.get_pos();

        errp=ref-out;
        up= errp > C2;

        errv=up-enc.get_vel();
//        uv = errv > C1;
        uv = errp > C1;
        drv.SetThrottle(uv);

        plot1.pushBack(enc.get_vel());
        plot2.pushBack(x[1][0]);


        Ts.WaitSamplingTime();
    }
    drv.SetThrottle(0);

    plot1.Plot();
    plot2.Plot();


    cout << "enc stopped at pulse: " << enc.pulses << endl;






    return 0;
}
