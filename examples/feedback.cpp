
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 216; // BOARD pin 7
const int pin_B = 74;  // BOARD pin 5

//const int pin_ain1 = 149;  // BOARD pin 29
//const int pin_ain2 = 200; // BOARD pin 31
//const int pin_stby = 13;   // BOARD pin 27
//const int num_pwm = 2;   // BOARD pin 33

const int pin_ain1 = 200;   // BOARD pin 31
const int pin_ain2 = 149; // BOARD pin 29
const int pin_stby = 13;  // BOARD pin 27
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
#include "fcontrol.h"
#include "IPlot.h"



//void (*const blinkPtr)(int);

using namespace std;

int main()
{

    double dts=0.01;

    GPIOJetson sbc;

    QuadEncoder enc(pin_A, pin_B, sbc, 28, dts);

    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);



    drv.enable();
//    drv.SetThrottle(100);


    SamplingTime Ts(dts); // en segundos

    PID c1(0.087,0.0154,0.00043,dts);

//    matlab G=tf([ 2.34325],[ 1, -0.803728],0.01)
//    matlab G=tf([ 1.52269],[ 1, -0.875576],0.01)
//    matlab G=tf([ 2.40925],[ 1, -0.79602],0.01)

    vector<double> num = {2.4,0};
    vector<double> den = {-0.8,1};
    TF P1(num,den);

    SS S1(P1);

    vector<vector<double>> G={ {0.8, 0} , {0.01, 1} };
    vector<vector<double>> H={ {2.4} , {0}};
    vector<vector<double>> C={ {0,1} };
    vector<vector<double>> D={ {0} };

    SS S2(G,H,C,D,dts);

    StateObserver O1(S2);
    vector<vector<double>> O1G={ {0.9}, {0.05} };
    O1.SetGainsK(O1G);

    LSFFT c2(P1,dts);
//    vector<double> c3n={0.002274,-0.01367,0.01163};
//    vector<double> c3d={0.999, -1.999, 1};





    vector<double> c3n={     -0.0015782 ,  0.0019452 ,  0.0089116 , -0.0188748 ,  0.0096545};
    vector<double> c3d={   0.2106804 , -1.4663576 ,  3.3003991 , -3.0447220  , 1.0000000};
    TF c3(c3n,c3d);

    vector<double> c4n={-0.0039672 ,  0.0250422 , -0.0190517,  -0.0325373 ,  0.0307162 };
    vector<double> c4d={ -0.2157920 ,  0.9476378 , -0.2627150 , -1.4691323 ,  1.0000000 };
    TF c4(c4n,c4d);


//    SS S3(c3);


    IPlot plot1(dts,"plot1"," x axis","y axis");
    IPlot plot2(dts,"plot2"," x axis","y axis");

    double out=0,in=0,pos=0;
    double tgt=100, err=0;

    vector<vector<double>> x={{0},{0}};

//    c1.AntiWindup(-100,100);

    for (int i=0;i<400;i++)
    {
//        out=enc.get_vel();
//        out=enc.GetFVel();
        out=enc.get_pos();


        err=tgt-out;

        in= err ;//> c1;

        drv.SetThrottle(in);
//        cout << "SetThrottle: " << in << endl;

        O1.Update(in,out);

        plot1.pushBack(out);
        x=O1.GetState();
        plot2.pushBack(x[0][0]);

        Ts.WaitSamplingTime();
    }
    drv.SetThrottle(0);

    plot1.Plot();
    plot2.Plot();





    return 0;
}
